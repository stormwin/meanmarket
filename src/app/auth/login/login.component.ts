import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Services
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm;
  error: String = "";

  constructor(private _router: Router, private _authService: AuthService) { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)] ),
      password: new FormControl('', Validators.required)
    });

  }

  // Try to login
  /*
    IR: On Successful Sign in => put Token in Local Storage
  */
  onLogin() {
    this._authService.signIn(this.loginForm.value)
    .subscribe(
      // (data) => { console.log(data); },
      (res) => {

        // Save the active User
        this._authService.activeUser = res['user'];

        // Put the token in LocalStorage
        localStorage.setItem( 'access-token' , res['token'] );
        this._router.navigate(['/myaccount']);
      },
      (err) => { this.error = err; }
    );
  }

}
